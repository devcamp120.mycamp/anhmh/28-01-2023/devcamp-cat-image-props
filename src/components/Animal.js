import { Component } from "react";
import cat from "../../src/assets/cat.jpeg"
class Animal extends Component {
    constructor(props) {
        super(props)

        this.state = {
            kind: "Cat Image"
        }
    }
    
    render() {
        if(this.state.kind) {
            return (
                <>
                    <img src={cat} className="Cat Image" alt=""/>
                </>
            )
        } else {
            return(
                <>
                    <p>Meow not found</p>
                </>
            )
        }
    }
}

export default Animal;